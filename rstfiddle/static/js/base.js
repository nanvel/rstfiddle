jQuery(function ($) {
    var rstfiddle = {};
    if (this.rstfiddle !== undefined) {
        rstfiddle = this.rstfiddle;
    } else {
        this.rstfiddle = rstfiddle;
    }
    rstfiddle.processor = {
        TIMEOUT: 5000,
        timeout: undefined,
        init: function() {
            rstfiddle.processor.url = $('#run').data('url');
            rstfiddle.processor.textarea = $('#rst > div > textarea');
            $('#run').on('click', function() {
                rstfiddle.processor.process();
                return false;
            });
            rstfiddle.processor.textarea.on('keyup', function(e) {
                rstfiddle.processor.resize();
                if (e.which > 90 || e.which < 48) return;
                if(rstfiddle.processor.timeout) {
                    clearTimeout(rstfiddle.processor.timeout);
                }
                rstfiddle.processor.timeout = setTimeout(function() {
                    rstfiddle.processor.process();
                }, rstfiddle.processor.TIMEOUT);
            });
            rstfiddle.processor.resize();
            if(rstfiddle.processor.textarea.val().length) rstfiddle.processor.process();
        },
        process: function() {
            $.ajax({
                url: rstfiddle.processor.url,
                dataType: 'html',
                data: {rst: rstfiddle.processor.textarea.val()},
                type: 'POST',
                success: function (data) {
                    $("#result > div").html(data)
                },
                beforeSend: function() {
                    rstfiddle.processor.spinner(true);
                },
                error: function() {
                    /* TODO */
                },
                complete: function() {
                    rstfiddle.processor.spinner(false);
                }
            });
        },
        spinner: function(show) {
            if(show) {
                $('#spinner').show();
            } else {
                $('#spinner').hide();
            }
        },
        resize: function() {
            var ta = rstfiddle.processor.textarea;
            ta.height(16);
            ta.height(ta.prop('scrollHeight') + 8);
        },
    }
    rstfiddle.processor.init();
});