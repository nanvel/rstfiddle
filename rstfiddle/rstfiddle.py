from flask import Flask, request, render_template, Markup

from docutils.core import publish_string

from settings import DEBUG


app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/rsttohtml/', methods=['POST'])
def rsttohtml():
    rst = request.form.get('rst', '')
    html = Markup(publish_string(source=rst, writer_name='html')).unescape()
    html = html[html.find('<body>') + 6: html.find('</body>')]
    return html


@app.errorhandler(404)
def page_not_found(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_server_error(error):
    return render_template('500.html'), 500


if __name__ == '__main__':
    app.debug = DEBUG
    app.run()
