#!/usr/bin/python
from flup.server.fcgi import WSGIServer
from rstfiddle import app
from settings import SOCKET


if __name__ == '__main__':
    WSGIServer(app, bindAddress=SOCKET).run()
