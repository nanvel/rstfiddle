import unittest

import rstfiddle


class RSTFiddleTestCase(unittest.TestCase):

    def setUp(self):
        self.app = rstfiddle.app.test_client()

    def tearDown(self):
        pass

    def test_index_view(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)

    def test_rst_to_hrml_view(self):
        response = self.app.post('/rsttohtml/', data={'rst': '123\n---'})
        self.assertEqual(response.status_code, 200)
        self.assertIn('<h1 class="title">123</h1>', response.data)


if __name__ == '__main__':
    unittest.main()
